<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
        <div class="Ptable">
            <div class="Ptable-item">
                <h3>曝光控制</h3>
                <dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>场景模式</dt>
                        <dd>肖像；风景；微距；运动；日落；夜景肖像；夜景</dd>
                    </dl>
                </dl>
            </div>
            <div class="Ptable-item">
                <h3>镜头参数</h3>
                <dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>滤镜直径</dt>
                        <dd>其他</dd>
                    </dl>
                </dl>
            </div>
            <div class="Ptable-item">
                <h3>存储参数</h3>
                <dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>存储介质</dt>
                        <dd>XQD卡</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>机身内存</dt>
                        <dd>根据XQD卡而定</dd>
                    </dl>
                </dl>
            </div>
            <div class="Ptable-item">
                <h3>屏幕参数</h3>
                <dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>液晶屏类型</dt>
                        <dd>旋转屏；触摸屏</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>取景器类型</dt>
                        <dd>电子取景器</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>液晶屏像素</dt>
                        <dd>约210万画点</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>液晶屏尺寸</dt>
                        <dd>3英寸以上</dd>
                    </dl>
                </dl>
            </div>
            <div class="Ptable-item">
                <h3>基本参数</h3>
                <dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>品牌</dt>
                        <dd>尼康（Nikon）</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>用途</dt>
                        <dd>人物摄影 风光摄影 运动抓拍</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>机身重量（g）</dt>
                        <dd>约585g，仅照相机机身</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>传感器类型</dt>
                        <dd class="Ptable-tips">
                            <a href="#none"><i class="Ptable-sprite-question"></i></a>
                            <div class="tips">
                                <div class="Ptable-sprite-arrow"></div>
                                <div class="content">
                                    <p>是数码的成像感光元件，与相机一体</p>
                                </div>
                            </div>
                        </dd>
                        <dd>CMOS</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>画幅</dt>
                        <dd>全画幅</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>型号</dt>
                        <dd>Z6</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>有效像素</dt>
                        <dd class="Ptable-tips">
                            <a href="#none"><i class="Ptable-sprite-question"></i></a>
                            <div class="tips">
                                <div class="Ptable-sprite-arrow"></div>
                                <div class="content">
                                    <p>是指真正参与感光成像的像素值</p>
                                </div>
                            </div>
                        </dd>
                        <dd>约2,450万</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>商品产地</dt>
                        <dd>日本</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>上市时间</dt>
                        <dd>2018/11</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>对焦系统</dt>
                        <dd>复合相位侦测/对比侦测自动对焦，带自动对焦辅助</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>商品毛重（kg）</dt>
                        <dd>0.74</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>尺寸（mm）</dt>
                        <dd>约134 x 100.5 x 67.5mm</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>传感器尺寸</dt>
                        <dd class="Ptable-tips">
                            <a href="#none"><i class="Ptable-sprite-question"></i></a>
                            <div class="tips">
                                <div class="Ptable-sprite-arrow"></div>
                                <div class="content">
                                    <p>即感光器件的面积大小</p>
                                </div>
                            </div>
                        </dd>
                        <dd>全画幅</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>类型</dt>
                        <dd>全画幅微单</dd>
                    </dl>
                </dl>
            </div>
            <div class="Ptable-item">
                <h3>电源参数</h3>
                <dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>电池类型</dt>
                        <dd>可充电锂离子电池</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>外接电源</dt>
                        <dd>支持</dd>
                    </dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>电池型号</dt>
                        <dd>1块EN-EL15b锂离子电池组；也可使用EN-EL15a/EN-EL15锂离子电池组，但是请注意，一次充电可拍摄的照片张数较少，而且可充电电源适配器仅可用于为EN-EL15b锂离子电池组充电</dd>
                    </dl>
                </dl>
            </div>
            <div class="Ptable-item">
                <h3>接口参数</h3>
                <dl>
                    <dl class="clearfix" style="margin:0">
                        <dt>WiFi连接</dt>
                        <dd>支持</dd>
                    </dl>
                </dl>
            </div>
        </div>
        <div class="package-list">
            <h3>包装清单</h3>
            <p>实际内容以产品外包装或说明书标示为准</p>
        </div>
</body>
</html>