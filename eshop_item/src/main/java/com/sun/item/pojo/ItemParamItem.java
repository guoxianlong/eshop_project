package com.sun.item.pojo;

public class ItemParamItem {

	private String group;
	private ItemParam[] params;
	
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public ItemParam[] getParams() {
		return params;
	}
	public void setParams(ItemParam[] params) {
		this.params = params;
	}
	
	
}
