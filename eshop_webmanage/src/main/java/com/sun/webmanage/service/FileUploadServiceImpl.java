package com.sun.webmanage.service;

import java.io.IOException;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sun.commons.utils.FtpUtils;
import com.sun.commons.utils.IDUtils;

@Service("FileUploadService")
public class FileUploadServiceImpl implements FileUploadService{
	@Value("${ftpclient.host}")
	private String host;
	@Value("${ftpclient.port}")
	private Integer port;
	@Value("${ftpclient.username}")
	private String username;
	@Value("${ftpclient.password}")
	private String password;
	@Value("${ftpclient.basePath}")
	private String basePath;
	@Value("${ftpclient.filepath}")
	private String filePath;
	@Value("${imageurl}")
	private String imgurl;
	
	//文件上传路径  basePath+filePath
	private final static Logger LOGGER = LoggerFactory.getLogger(FileUploadService.class);

	@Override
	public String fileUpload(MultipartFile file) {
		//String imageName = IDUtils.genImageName();
		String fileName = UUID.randomUUID()+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
		String imgpath = "";
		try {
			boolean status = FtpUtils.uploadFile(host, port, username, password, basePath, filePath, fileName, file.getInputStream());
		    if(status){
		    	imgpath = imgurl + fileName;
		    	return imgpath;
		    }
		} catch (IOException e) {
			LOGGER.error("fileUpload error==="+e.getMessage());
			return "";
		}
		return "";
	}

	
}
