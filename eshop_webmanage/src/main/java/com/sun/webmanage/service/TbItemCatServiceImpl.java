package com.sun.webmanage.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.sun.commons.pojo.EasyUITreeData;
import com.sun.dubbo.webmanage.service.TbItemCatDubboService;
import com.sun.webmanage.model.TbItemCat;
import com.sun.webmanage.pojo.TbItemCatCustom;

@Service("TbItemCatService")
public class TbItemCatServiceImpl implements TbItemCatService{

	@Reference
	private TbItemCatDubboService tbItemCatDubboService;

	@Override
	public List<EasyUITreeData> listEasyUITreeData(long pid) {
		List<TbItemCat> listdata = tbItemCatDubboService.listTbItemCatByPid(pid);
		List<EasyUITreeData> retList = new ArrayList<>();
		EasyUITreeData tree = null;
		for(TbItemCat cat:listdata){
			tree = new EasyUITreeData();
			tree.setId(cat.getId());
			tree.setText(cat.getName());
			tree.setState(cat.getIsParent()?"closed":"open");
			retList.add(tree);
		}
		return retList;
	}

	@Override
	public boolean setCatPath() {
		List<TbItemCat> listdata = tbItemCatDubboService.listTbItemCatByPid(0);
		listTbItemCat("",listdata);
		return true;
	}
	
	private void listTbItemCat(String pre,List<TbItemCat> parentList){
		for(TbItemCat cat:parentList){
			cat.setPath(pre+cat.getId());
			List<TbItemCat> childlist = tbItemCatDubboService.listTbItemCatByPid(cat.getId());
			if(childlist!=null && !childlist.isEmpty()){
				listTbItemCat(cat.getPath()+"||",childlist);
			}
			tbItemCatDubboService.upadteTbItemCat(cat);
		}
	}
	
	
}
