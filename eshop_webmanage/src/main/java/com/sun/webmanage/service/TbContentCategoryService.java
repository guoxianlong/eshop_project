package com.sun.webmanage.service;

import java.util.List;

import com.sun.commons.pojo.EasyUITreeData;
import com.sun.exception.DaoException;
import com.sun.webmanage.model.TbContentCategory;

public interface TbContentCategoryService {

	List<EasyUITreeData> listTbContentCategory(long pid);
	
	TbContentCategory saveTbContentCategory(Long parentId,String name)throws DaoException;
	
	boolean updateTbContentCategory(Long id,String name) throws DaoException;
	
	boolean deleteTbContentCategory(Long parentId,Long id);
	
}
