<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div style="padding:5px;background:#fafafa;width:100%;border:1px solid #ccc">
        <a href="javascript:void(0);" id="refreshSolr" class="easyui-linkbutton" iconCls="icon-reload">刷新商品搜索信息缓存</a>
        <input type="radio" name="isall" value="false" alt="更新到新" checked/>更新到新
        <input type="radio" name="isall" value="true" alt="重置索引"/>*重置索引
        <a href="javascript:void(0);" class="easyui-linkbutton" plain="true" iconCls="icon-help" onClick="help()"></a>
</div>
<script>
function help(){
	$.messager.alert('提示','本次操作将会更新商品搜索中的索引信息<br><b>更新最新：</b>表示将商品信息从上次更新的记录更新到最新的记录<br><b>重置索引：</b>表示将所有商品信息都重新更新一遍');
}
$("#refreshSolr").click(function(e){
	var is_all = $("input[name=isall]").val();
	$.getJSON('reflash/solrcache/'+is_all,function(data){
		if(data.status == '200'){
			//{"data":"更新条目数：3500,操作时间：77s","status":200,"info":"同步solr索引信息成功"}
			$.messager.alert("提示",data.info+"<br>"+data.data);
		}else{
			$.messager.alert("提示","操作失败，请刷新重试");
		}
	});
});
</script>
