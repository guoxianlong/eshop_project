package com.sun.dubbo.webmanage.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.sun.dubbo.webmanage.service.TbUserDubboService;
import com.sun.exception.DaoException;
import com.sun.webmanage.mapper.TbUserMapper;
import com.sun.webmanage.model.TbUser;
import com.sun.webmanage.model.TbUserExample;

public class TbUserDubboServiceImpl implements TbUserDubboService{

	@Resource
	private TbUserMapper tbUserMapper;
	
	@Override
	public TbUser loadTbUserByUsernameAndPassword(String username, String password) {
		TbUserExample example = new TbUserExample();
		example.createCriteria().andUsernameEqualTo(username).andPasswordEqualTo(password);
		List<TbUser> userlist = tbUserMapper.selectByExample(example);
		if(userlist!=null && !userlist.isEmpty()){
			return userlist.get(0);
		}
		return null;
	}

	@Override
	public boolean insertTbUser(TbUser tbUser)throws DaoException {
		String username = tbUser.getUsername();
		if(username==null || username.equals("")){
			throw new DaoException("用户名不允许为空");
		}
		TbUserExample example = new TbUserExample();
		example.createCriteria().andUsernameEqualTo(username);
		long countByExample = tbUserMapper.countByExample(example);
		if(countByExample>0){
			throw new DaoException("该用户名已经被注册");
		}
		Date now = new Date();
		tbUser.setCreated(now);
		tbUser.setUpdated(now);
		
		if(tbUserMapper.insertSelective(tbUser)>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean updateTbUser(TbUser tbUser) {
		if(tbUserMapper.updateByPrimaryKeySelective(tbUser)>0){
			return true;
		}
			
		return false;
	}

}
