package com.sun.dubbo.webmanage.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.dubbo.webmanage.service.TbContentDubboService;
import com.sun.webmanage.mapper.TbContentMapper;
import com.sun.webmanage.model.TbContent;
import com.sun.webmanage.model.TbContentExample;

public class TbContentDubboServiceImpl implements TbContentDubboService{
	
	@Resource
	private TbContentMapper tbContentMapper;

	@Override
	public EasyUIDataGrid listTbContentPage(int pager, int rows, long cid) {
		TbContentExample example = new TbContentExample();
		if(cid!=0){
			example.createCriteria().andCategoryIdEqualTo(cid);
		}
		PageHelper.startPage(pager, rows);
		List<TbContent> listdata = tbContentMapper.selectByExampleWithBLOBs(example);
		PageInfo<TbContent> page = new PageInfo<>(listdata);
		EasyUIDataGrid grid = new EasyUIDataGrid();
		grid.setRows(listdata);
		grid.setTotal(page.getTotal());
		return grid;
	}

	@Override
	public Long saveTbContent(TbContent tbContent) {
		if(tbContentMapper.insertSelective(tbContent)>0){
			return tbContent.getId();
		}
		return null;
	}

	@Override
	public boolean editTbContent(TbContent tbContent) {
		if(tbContentMapper.updateByPrimaryKeyWithBLOBs(tbContent)>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteTbContent(long id) {
		if(tbContentMapper.deleteByPrimaryKey(id)>0){
			return true;
		}
		return false;
	}

	@Override
	public List<TbContent> listTbContentLimit(long category_id,int limit, boolean is_sort) {
		TbContentExample example = new TbContentExample();
		example.createCriteria().andCategoryIdEqualTo(category_id);
		if(is_sort){//添加排序列
			example.setOrderByClause("updated desc");
		}
		if(limit!=0){
			PageHelper.startPage(1, limit);
			List<TbContent> tbContents = tbContentMapper.selectByExampleWithBLOBs(example);
			PageInfo<TbContent> page = new PageInfo<>(tbContents);
			return page.getList();
		}else{
			return tbContentMapper.selectByExampleWithBLOBs(example);
		}
	}

}
