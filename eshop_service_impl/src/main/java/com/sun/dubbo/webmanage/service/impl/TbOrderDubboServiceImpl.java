package com.sun.dubbo.webmanage.service.impl;

import javax.annotation.Resource;

import com.sun.dubbo.webmanage.service.TbOrderDubboService;
import com.sun.webmanage.mapper.TbOrderMapper;
import com.sun.webmanage.model.TbOrder;

public class TbOrderDubboServiceImpl implements TbOrderDubboService{
	
	@Resource
	private TbOrderMapper tbOrderMapper;

	@Override
	public boolean insertTbOrder(TbOrder tbOrder) {
		if(tbOrderMapper.insertSelective(tbOrder)>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean updateTbOrder(TbOrder tbOrder) {
		if(tbOrderMapper.updateByPrimaryKeySelective(tbOrder)>0){
			return true;
		}
		return false;
	}

}
