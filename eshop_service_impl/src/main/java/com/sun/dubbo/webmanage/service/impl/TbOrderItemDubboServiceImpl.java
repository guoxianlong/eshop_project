package com.sun.dubbo.webmanage.service.impl;

import javax.annotation.Resource;

import com.sun.dubbo.webmanage.service.TbOrderItemDubboService;
import com.sun.webmanage.mapper.TbOrderItemMapper;
import com.sun.webmanage.model.TbOrderItem;

public class TbOrderItemDubboServiceImpl implements TbOrderItemDubboService{
	
	@Resource
	private TbOrderItemMapper tbOrderItemMapper;

	@Override
	public boolean insertTbOrderItem(TbOrderItem tbOrderItem) {
		if(tbOrderItemMapper.insertSelective(tbOrderItem)>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean updateTbOrderItem(TbOrderItem tbOrderItem) {
		if(tbOrderItemMapper.updateByPrimaryKeySelective(tbOrderItem)>0){
			return true;
		}
		return false;
	}

}
