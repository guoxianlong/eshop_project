package com.sun.dubbo.webmanage.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.dubbo.webmanage.service.TbItemParamDubboService;
import com.sun.webmanage.mapper.TbItemParamMapper;
import com.sun.webmanage.model.TbItemParam;
import com.sun.webmanage.model.TbItemParamExample;

public class TbItemParamDubboServiceImpl implements TbItemParamDubboService{

	@Resource
	private TbItemParamMapper tbItemParamMapper;
	
	
	@Override
	public EasyUIDataGrid listTbItemParamPage(int pager, int rows) {
		PageHelper.startPage(pager, rows);
		List<TbItemParam> listdata = tbItemParamMapper.selectByExampleWithBLOBs(new TbItemParamExample());
		PageInfo<TbItemParam> page = new PageInfo<>(listdata);
		EasyUIDataGrid retdata = new EasyUIDataGrid();
		retdata.setRows(listdata);
		retdata.setTotal(page.getTotal());
		return retdata;
	}


	@Override
	public int delTbItemParamByPK(long id) {
		return tbItemParamMapper.deleteByPrimaryKey(id);
	}


	@Override
	public List<TbItemParam> loadParamByCatId(long catId) {
		TbItemParamExample example = new TbItemParamExample();
		example.createCriteria().andItemCatIdEqualTo(catId);
		List<TbItemParam> listdata = tbItemParamMapper.selectByExampleWithBLOBs(example);
		return listdata;
	}


	@Override
	public int saveTbItemParam(TbItemParam tbItemParam) {
		return tbItemParamMapper.insertSelective(tbItemParam);
	}

}
