package com.sun.dubbo.webmanage.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.sun.dubbo.webmanage.service.TbContentCategoryDubboService;
import com.sun.exception.DaoException;
import com.sun.webmanage.mapper.TbContentCategoryMapper;
import com.sun.webmanage.model.TbContentCategory;
import com.sun.webmanage.model.TbContentCategoryExample;

public class TbContentCategoryDubboServiceImpl implements TbContentCategoryDubboService{

	@Resource
	TbContentCategoryMapper tbContentCategoryMapper;

	@Override
	public List<TbContentCategory> listContentCatByPid(long pid) {
		TbContentCategoryExample example = new TbContentCategoryExample();
		example.createCriteria().andParentIdEqualTo(pid).andStatusEqualTo(1);
		return tbContentCategoryMapper.selectByExample(example);
	}

	@Override
	public TbContentCategory insertTbContentCategory(TbContentCategory tbContentCategory,Long pid) throws DaoException{
		List<TbContentCategory> childList = listContentCatByPid(pid);
		int count = childList.size();
		if(count>0){
			tbContentCategory.setSortOrder((int)count+1);
			/*long count2 = childList.stream().filter(x->{
				return x.getName().equals(tbContentCategory.getName());
			}).count();
			if(count2>0){
				throw new DaoException("当前分类下子分类重名");
			}*/
			for(TbContentCategory category:childList){
				if(category.getName().equals(tbContentCategory.getName())){
					throw new DaoException("当前分类下子分类重名");
				}
			}
		}else{
			tbContentCategory.setSortOrder(1);
			TbContentCategory parent = loadContentCategoryByPK(pid);
			parent.setIsParent(true);
			if(updateTbContentCategory(parent)<=0){
				throw new DaoException("更新父类目异常");
			}
		}
		
		if(tbContentCategoryMapper.insertSelective(tbContentCategory)<=0){
			throw new DaoException("新增类目异常");
		}
			return tbContentCategory;
	}

	@Override
	public int updateTbContentCategory(TbContentCategory tbContentCategory) throws DaoException {
		TbContentCategory selectByPrimaryKey = tbContentCategoryMapper.selectByPrimaryKey(tbContentCategory.getId());
		List<TbContentCategory> childList = listContentCatByPid(selectByPrimaryKey.getParentId());
		if(childList!=null && childList.size()>1){
			/*long count2 = childList.stream().filter(x->{
				return x.getName().equals(tbContentCategory.getName());
			}).count();
			if(count2>0){
				throw new DaoException("当前分类下子分类重名");
			}*/
			for(TbContentCategory category:childList){
				if(category.getName().equals(tbContentCategory.getName())){
					throw new DaoException("当前分类下子分类重名");
				}
			}
		}
		return tbContentCategoryMapper.updateByPrimaryKeySelective(tbContentCategory);
	}

	@Override
	public boolean deleteTbContentCategory(Long parentId,Long id) throws DaoException{
		if(parentId == null){//如果是一级类目   删除自己及子类目
			List<TbContentCategory> listchild = listContentCatByPid(id);
			for(TbContentCategory child:listchild){
				child.setStatus(2);
				if(tbContentCategoryMapper.updateByPrimaryKeySelective(child)<=0){
					throw new DaoException("删除失败");
				}
			}
			TbContentCategory self = loadContentCategoryByPK(id);
			self.setStatus(2);
			if(tbContentCategoryMapper.updateByPrimaryKeySelective(self)<=0){
				throw new DaoException("删除失败");
			}
			return true;
		}
		
		//如果不是一级类目  删除自己  判断父类是否应该更新为叶子类目
		TbContentCategory self = loadContentCategoryByPK(id);
		self.setStatus(2);
		if(tbContentCategoryMapper.updateByPrimaryKeySelective(self)<=0){
			throw new DaoException("删除失败");
		}
		
		long count = countChildCategoryByPid(parentId);
		if(count <= 0){
			TbContentCategory parent = loadContentCategoryByPK(parentId);
			parent.setIsParent(false);
			if(tbContentCategoryMapper.updateByPrimaryKeySelective(parent)<=0){
				throw new DaoException("删除失败");
			}
		}
		return true;
	}

	@Override
	public long countChildCategoryByPid(long pid) {
		TbContentCategoryExample example = new TbContentCategoryExample();
		example.createCriteria().andParentIdEqualTo(pid);
		return tbContentCategoryMapper.countByExample(example);
	}

	@Override
	public TbContentCategory loadContentCategoryByPK(long pid) {
		return tbContentCategoryMapper.selectByPrimaryKey(pid);
	}
	
	
}
