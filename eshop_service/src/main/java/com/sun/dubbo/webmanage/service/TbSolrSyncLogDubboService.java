package com.sun.dubbo.webmanage.service;

import java.util.List;

import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.webmanage.model.TbSolrSyncLog;

public interface TbSolrSyncLogDubboService {

	int insertSolrSyncLog(TbSolrSyncLog record);
	
	EasyUIDataGrid selectSolrSyncLogList(int page,int rows,Byte operator_type);
	
	/**根据操作类型type查找最新前10条记录
	 * @return
	 */
	List<TbSolrSyncLog> selectSolrSyncLogNewByType(byte operator_type);
}
