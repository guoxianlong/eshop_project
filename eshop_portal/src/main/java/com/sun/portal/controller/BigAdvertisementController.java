package com.sun.portal.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sun.portal.service.TbContentPortalService;

@Controller
public class BigAdvertisementController {

	@Resource
	private TbContentPortalService tbContentPortalService;
	
	@RequestMapping("getAdData")
	public String showBigAdvertisement(Model retmodel){
		String jsonstr = tbContentPortalService.listBigAdvertisement();
		retmodel.addAttribute("ad1", jsonstr);
		return "index";
	}
}
